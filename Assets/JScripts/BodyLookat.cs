﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using GoogleARCore;

public class BodyLookat : MonoBehaviour {

	public GameObject Target;
	public Vector3 Targetpos;
	public GameObject Master;
	public GameObject ball;
	public GameObject ball_2;
	private GameObject ball_2clone;
	public GameObject ballicon;
	public GameObject comeicon;
    public GameObject rebornicon;
    public GameObject Masterpos;
	public GameObject ball2_pos;

	public GameObject detect_obj;
	private Detect detect_sc;
	private HeadLookController hdlook;

	private Animator ani;

	private NavMeshAgent agent;

	public bool decpos = false;
	public bool come = false;
	public bool Pick = false;
	public bool follow = false;

	// Use this for initialization
	void Start () 
	{
		detect_sc = detect_obj.GetComponent<Detect> ();
		ani = this.GetComponent<Animator> ();
		//agent = GetComponent<NavMeshAgent>();
		hdlook = GetComponent<HeadLookController>();
		Masterpos_curhit = Masterpos.GetComponent<CursorHit> ();
		ball_curhit = ball.GetComponent<CursorHit> ();
	}
		
	Vector3 targetDir;
	Vector3 newDir;

	Vector3 targetDir_1;
	Vector3 newDir_1;

	public bool scare = false;
	public float dis;
	public int do_once_1 = 0;

	private CursorHit Masterpos_curhit;
	private CursorHit ball_curhit;

	public int x;

	// Update is called once per frame

	void Update () 
	{

		Master.transform.position = new Vector3 (Target.transform.position.x,transform.position.y,Target.transform.position.z);
		if (ball.activeSelf) 
		{
			if (do_once_1 == 0)
			{
				ball_curhit.enabled = true;
				Masterpos_curhit.enabled = false;
				Target = ball;
				come = false;
				detect_sc.see = false;
				follow = true;
				do_once_1 = 1;
				//x = Random.Range (0, 2);
				x = 0;
			}
		} 

		if (Fly)
		{
			transform.rotation = Quaternion.LookRotation (newDir);
		}
			
		if (scare) 
		{
			hdlook.enabled = false;
		} 
		else if (scare == false)
		{
			Watch ();
			//hdlook.enabled = true;
		}
		if (Pick)
		{
			if (x == 0)
			{
				come = true;
			}
//			if (x == 1 && decpos == false && Pick == true)
//			{
//				ani.SetTrigger ("UBFire");
//				Pick = false;
//				StartCoroutine (shooting ());
//			}
		}
		//Scare ();
	}

	//注視跟轉身
	void Watch ()
	{
		dis = Vector3.Distance(Master.transform.position,transform.position);
		targetDir = Targetpos - transform.position;
		targetDir.y = 0;
		newDir = Vector3.RotateTowards (transform.forward, targetDir, 0.2f, 0.0F);

		targetDir_1 = Target.transform.position - transform.position;
		targetDir_1.y = 0;
		newDir_1 = Vector3.RotateTowards (transform.forward, targetDir_1, 0.2f, 0.0F);

		//轉頭
		if (detect_sc.see) 
		{
			if (follow && Turn == false && decpos == false)
			{
				if (dis <= 0.05f) 
				{
					ani.SetBool ("Walk", false);
					ani.SetBool ("Pick", true);
					ball_curhit.enabled = false;
					StartCoroutine (seeback ());
				} 
				else 
				{
					ani.SetBool ("Walk",true);
					transform.rotation = Quaternion.LookRotation (newDir_1);
					transform.Translate (Vector3.forward * 0.3f*Time.deltaTime, Space.Self);
				}
			}
			if (come && Turn == false && decpos == false && follow == false)
			{
				ani.SetBool ("Walk",true);
				transform.rotation = Quaternion.LookRotation (newDir_1);
				transform.Translate (Vector3.forward * 0.3f*Time.deltaTime, Space.Self);
				if (dis <= 0.3f) 
				{
					ball_2.SetActive (false);
					ani.SetBool ("Walk",false);
					ani.SetBool ("Pick", false);
					ballicon.SetActive (true);
					comeicon.SetActive (true);
                    rebornicon.SetActive(true);
                    //ani.SetLayerWeight (1, 0);
                    come = false;
					Pick = false;
					do_once_1 = 0;
				}
			}
		} 
		else 
		{
			ani.SetBool ("Walk",false);

			if (decpos == false)
			{
				StartCoroutine (turnbody (1.3f));
				decpos = true;
			}
		}
	}
	public IEnumerator shooting()
	{
		yield return new WaitForSeconds (50f*Time.deltaTime);
		ani.SetTrigger ("Fire");
		ball_2clone = Instantiate (ball_2);
		ani.SetBool ("Pick", false);
		ball_2.GetComponent<Rigidbody> ().AddRelativeForce (Vector3.forward * 300f);
		do_once_1 = 0;
	}
	public IEnumerator turnbody(float x)
	{
		yield return new WaitForSeconds (x);
		Targetpos = Target.transform.position;
		ani.SetBool ("Fly",true);
		Turn = true;
	}
	public IEnumerator seeback()
	{
		yield return new WaitForSeconds (20f*Time.deltaTime);
		Masterpos_curhit.enabled = true;
		Target = Masterpos;
		ball.SetActive (false);
		ball_2.SetActive (true);
		yield return new WaitForSeconds (50f*Time.deltaTime);
//		come = true;
//		ani.SetBool ("Pick",false);
		ani.SetBool ("Fly",true);
		follow = false;
		Pick = true;

//		ani.SetLayerWeight (1, 0.5f);
//		yield return new WaitForSeconds (7f*Time.deltaTime);
//		ani.SetLayerWeight (1, 1);
	}
	//驚嚇
	void Scare ()
	{
		if (Frame.LightEstimate.PixelIntensity < 0.15f)
		{
			scare = true;
			ani.SetBool ("Scare",true);
		}
		if (Frame.LightEstimate.PixelIntensity >= 0.15f)
		{
			scare = false;
			ani.SetBool ("Scare",false);
		}
	}
//	void Fire_fn ()
//	{
//		ball_2.GetComponent<Rigidbody> ().useGravity = true;
//		ball_2.GetComponent<Rigidbody> ().AddRelativeForce (Vector3.forward * 10f);
//	}
	public void Come()
	{
		come = true;
	}
	private bool Fly;
	public bool Turn;
	public bool returnball;
	public void Fly_fn(int x)
	{
		if (x == 0)
		{
			Fly = true;
		}
		if (x == 1)
		{
			Fly = false;
			ani.SetBool ("Fly",false);
			decpos = false;
			Turn = false;
		}
	}
}
