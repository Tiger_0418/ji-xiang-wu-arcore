﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lookat_1 : MonoBehaviour {

	public BodyLookat sc;

	Vector3 targetDir;
	Vector3 newDir;

	public GameObject tar;

	public float limit_y = 310f;
	public float max_y = 340f;

	public float reset_r = 0f;
	public float reset_a = 20f;
	public float reset_z = 180f;
	public float reset_y = 200f;


	public float dd = -20f;
	public float zz = -50f;
	public float reset = 0;

	public float an_y;
	public float an_x;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		targetDir = sc.Target.transform.position - transform.position;
		newDir = Vector3.RotateTowards (transform.forward, targetDir, 0.09f, 0.0F);
		transform.rotation = Quaternion.LookRotation (newDir);

		an_y = transform.localEulerAngles.y;
		an_x = transform.localEulerAngles.x;

//		if (transform.localEulerAngles.x < limit_x_1) 
//		{
//			tar.transform.localEulerAngles = new Vector3 (limit_x_1, transform.localEulerAngles.y, 0);
//		} 
//		if (transform.localEulerAngles.x > max_x_1) 
//		{
//			tar.transform.localEulerAngles = new Vector3 (max_x_1, transform.localEulerAngles.y, 0);
//			if (transform.localEulerAngles.y > limit_y && transform.localEulerAngles.y < max_y) 
//			{
//				tar.transform.localEulerAngles = new Vector3 (max_x_1, transform.localEulerAngles.y, 0);
//			}
//		}
//		else 
//		{
//			tar.transform.localEulerAngles = new Vector3(transform.localEulerAngles.x,transform.localEulerAngles.y,0);
//		}


		if (transform.localEulerAngles.y > limit_y && transform.localEulerAngles.y < max_y) 
		{
			tar.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
			if (transform.localEulerAngles.x < -10)
			{
				tar.transform.localEulerAngles = new Vector3 (-10, transform.localEulerAngles.y, 0);
			} 
			if (transform.localEulerAngles.x < 10)
			{
				tar.transform.localEulerAngles = new Vector3 (10, transform.localEulerAngles.y, 0);
			} 
		}
		if (transform.localEulerAngles.y > reset_r && transform.localEulerAngles.y < reset_a)
		{
			tar.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, dd, 0);
		}
		if (transform.localEulerAngles.y > reset_z && transform.localEulerAngles.y < reset_y)
		{
			tar.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, zz, 0);
		} 
	}
}