﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

public class Lighttest : MonoBehaviour {

	[Range(0f,1f)]
	public float value = 0.5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		setGlobalLightEstimation (Frame.LightEstimate.PixelIntensity);
	}
	void OnValidate () 
	{
		setGlobalLightEstimation (value);
	}
	void setGlobalLightEstimation (float lightvalue) 
	{
		Shader.SetGlobalFloat ("_GlobalLightEstimation", lightvalue);
	}
}
