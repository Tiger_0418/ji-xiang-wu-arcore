﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ball : MonoBehaviour {
	
//	public GameObject ballpos;
//	public GameObject cam;
//	public GameObject tar;
	public Vector3 reset_pos;
	private Vector3 offset;
	private Vector3 screenPoint;
	private Vector3 cursorPosition;
	public float dis;
	Rigidbody rigi;

	public float ypos;

	public bool arnd;
	public float speed;
	public bool fire;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
//		if (arnd)
//		{
//			this.transform.RotateAround (tar.transform.position, Vector3.up,speed); 
//		}


	}
	private void OnMouseDown()
	{
		Debug.Log ("Down");
		reset_pos = gameObject.transform.localPosition;
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	}
	private void OnMouseDrag()
	{
		Vector3 cursorPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		cursorPosition = Camera.main.ScreenToWorldPoint (cursorPoint) + offset;
		transform.position = cursorPosition;
		if (transform.localPosition.y > ypos) {
			transform.localPosition = new Vector3 (transform.localPosition.x, ypos, transform.localPosition.z);
		}
		if (transform.localPosition.y <= ypos) {
			transform.localPosition = new Vector3 (0, transform.localPosition.y, transform.localPosition.z);
		}
	}
	private void OnMouseUp()
	{
		Debug.Log ("Up");
		if (transform.localPosition.y < ypos) 
		{
			dis = transform.localPosition.y - (ypos);
			rigi = GetComponent<Rigidbody> ();
			rigi.useGravity = true;
			rigi.AddRelativeForce(Vector3.up*Mathf.Abs(dis)*700f);
			rigi.AddRelativeForce(Vector3.forward*Mathf.Abs(dis)*200f);
			StartCoroutine (WaitAndsetfalse(3f));
		}
	}
	private IEnumerator WaitAndsetfalse(float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		gameObject.transform.localPosition = reset_pos;
		rigi.useGravity = false;
		rigi.velocity = new Vector3(0,0,0);
		transform.rotation = new Quaternion (0,0,0,0);
		gameObject.SetActive (false);
//		transform.SetParent(cam.transform);
		gameObject.SetActive(false);
	}

//	public Text pick;
//	void OnTriggerEnter(Collider other) 
//	{
//		pick.text = other.name;
//	}
}
